public class Banco {

	public static void main(String[] args) {
		double saldoAtual;
		
		ContaCorrente conta = new ContaCorrente(835.0f);
		
		saldoAtual = conta.getSaldo();
		
		Cliente cliente1 = new Cliente(conta);
		Fornecedor fornecedor1 = new Fornecedor(conta);
		
		System.out.println("Saldo Inicial: " + saldoAtual);
		cliente1.start();
		fornecedor1.start();
		
		while(cliente1.getState() != Thread.State.TERMINATED || fornecedor1.getState() != Thread.State.TERMINATED);
		
		System.out.println("Saldo Final = " + conta.getSaldo());

	}

}












